package com.xin;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class Qy158SpringbootRedisApplicationTests {

    //该类中已经封装了很多api方法，对redis服务操作的方法
    @Autowired
    private StringRedisTemplate redisTemplate;

    //关于对key操作的方法
    @Test
    void contextLoads() {
        //删除指定的key
        Boolean delete = redisTemplate.delete("k1");
        System.out.println("是否删除成功: " + delete);

        //判断指定的key是否存在
        Boolean hasKey = redisTemplate.hasKey("k1");
        System.out.println("指定的key是否存在: " + hasKey);
    }

    //测试关于对字符串的操作
    //在RedisTemplate类中，对于每种数据类型，都封装了相应的类对象，得到相应的类对象，即可对相应的数据类型进行操作。
    @Test
    public void test01() {
        //获取对String类型操作的类对象
        ValueOperations<String, String> forValue =  redisTemplate.opsForValue();
        //使用上面的类对象，即可对String类型进行操作
        forValue.set("k1","哈哈弹吉他");
        forValue.set("k2","嘻嘻弹钢琴",15, TimeUnit.MINUTES);

        //如果存在则不存入，不存在则存入
        Boolean aBoolean = forValue.setIfAbsent("K3", "啦啦做运动", 30, TimeUnit.SECONDS);
        System.out.println("是否存入成功: " + aBoolean);

        //获取对应的值
        String value = forValue.get("k1");
        System.out.println("获取k1对应的值: " + value);


        Long increment = forValue.increment("k4");
        System.out.println("递增后的结果为:" + increment);
    }

    //测试关于对hash类型的操作
    @Test
    public void test02() {
        HashOperations<String, Object, Object> forHash = redisTemplate.opsForHash();
        //存入
        forHash.put("k1","name","周杰伦");
        forHash.put("k1","age","21");
        forHash.put("k1","address","台湾");

        Map<String,String> map = new HashMap<>();
        map.put("name","陈奕迅");
        map.put("age","22");
        map.put("address","香港");
        forHash.putAll("k2",map);

        //取出
        Object o = forHash.get("k2", "name");
        System.out.println("获取指定key中对应的name的值:" + o);

        Map<Object, Object> map1 = forHash.entries("k1");
        System.out.println("获取k1对应的map对象:" + map1);

        Set<Object> keys = forHash.keys("k1");
        System.out.println("获取k1对应的所有的field: " + keys);

        List<Object> values = forHash.values("k1");
        System.out.println("获取k1对应的所有field的值:" + values);
    }


}
