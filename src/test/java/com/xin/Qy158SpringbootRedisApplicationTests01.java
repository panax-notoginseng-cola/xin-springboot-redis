package com.xin;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@SpringBootTest
class Qy158SpringbootRedisApplicationTests01 {

    @Autowired
    private RedisTemplate redisTemplate;

    // @Test
    // void contextLoads() {
    //     //默认的RedisTemplate，它的key和value的序列化都是默认使用的使用的JdkSerializationRedisSerializer方式
    //     // 该序列化要求类必须实现Serializable接口
    //     // 在实际开发中，key都是String类型，应该指定String序列化方式
    //     redisTemplate.setKeySerializer(new StringRedisSerializer());
    //     redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());
    //     ValueOperations valueOperations = redisTemplate.opsForValue();
    //     //会出现错误，可进行序列化
    //     valueOperations.set("k1",new User("哈哈",21,"郑州"));
    //     System.out.println(valueOperations.get("k1"));
    // }


//--------------------12.7-----------------------------------------

    @Test
    void contextLoads() {

        ValueOperations valueOperations = redisTemplate.opsForValue();
        valueOperations.set("k1",new User("哈哈",21,"郑州"));
        System.out.println(valueOperations.get("k1"));
    }
    @Test
    public void test01() {

        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put("k2","name","嘻嘻");
    }



}
