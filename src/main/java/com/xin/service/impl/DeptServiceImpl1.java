// package com.xin.service.impl;
//
// import com.xin.dao.DeptDao;
// import com.xin.entity.Dept;
// import com.xin.service.DeptService;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.cache.annotation.Cacheable;
// import org.springframework.data.redis.core.RedisTemplate;
// import org.springframework.data.redis.core.ValueOperations;
// import org.springframework.stereotype.Service;
//
// import java.util.concurrent.TimeUnit;
//
// /**
//  * @author ：Student王心
//  * @date ：Created in 2022/12/7 16:48
//  * @description：
//  * @modified By：
//  * @version:
//  */
// @Service
// public class DeptServiceImpl1 implements DeptService {
//
//     @Autowired
//     private DeptDao deptDao;
//     @Autowired
//     private RedisTemplate<String,Object> redisTemplate;
//
//
//     @Override
//     public Dept findById(Integer id) {
//         ValueOperations<String, Object> forValue = redisTemplate.opsForValue();
//         //查询缓存 ---key的名称: 【模块名称::类名::唯一主键】
//         Object o = forValue.get("dept::" + id);
//         //缓存不为空
//         if(o != null ){
//             return (Dept) o;
//         }
//         //缓存等于空，查询数据库
//         Dept dept = deptDao.selectById(id);
//         //数据库查询到数据
//         if(dept != null){
//             //数据库数据放入缓存以便下次命中
//             //不加时间，缓存中的数据会越来越多
//             forValue.set("dept::"+id,dept,3600, TimeUnit.SECONDS);
//         }
//         return dept;
//     }
//
//     @Override
//     public Integer delete(Integer id) {
//         //先删除缓存
//         //删除缓存中指定的key
//         redisTemplate.delete("dept::" +id);
//         //再删除数据库
//         int i = deptDao.deleteById(id);
//         return i;
//     }
//
//     @Override
//     public Dept save(Dept dept) {
//         //先不用考虑缓存
//          deptDao.insert(dept);
//         return dept;
//     }
//
//     @Override
//     public Dept update(Dept dept) {
//         //只修改了数据库,但是缓存中的数据还是之前的
//         //先修改数据库，再改缓存
//         deptDao.updateById(dept);
//
//         //修改缓存
//         ValueOperations<String, Object> forValue = redisTemplate.opsForValue();
//         forValue.set("dept::"+dept.getId(),dept,3600,TimeUnit.SECONDS);
//         return dept;
//
//
//     }
// }