package com.xin.service.impl;

import com.xin.dao.DeptDao;
import com.xin.entity.Dept;
import com.xin.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author ：Student王心
 * @date ：Created in 2022/12/7 16:48
 * @description：
 * @modified By：
 * @version:
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     * cacheNames:缓存的名称
     * key :表示唯一区分 #id读取方法的参数名
     * @param id
     * @return
     */
    @Override
    @Cacheable(cacheNames = "redis::dept",key = "#id")
    public Dept findById(Integer id) {

        Dept dept = deptDao.selectById(id);
        return dept;
    }

    @Override
    @CacheEvict(cacheNames = "redis::dept",key = "#id")
    public Integer delete(Integer id) {

        int i = deptDao.deleteById(id);
        return i;
    }

    @Override
    public Dept save(Dept dept) {

         deptDao.insert(dept);
        return dept;
    }

    @Override
    @CachePut(cacheNames = "redis::dept",key = "#dept.id")
    public Dept update(Dept dept) {

        deptDao.updateById(dept);
        return dept;


    }
}