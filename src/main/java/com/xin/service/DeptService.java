package com.xin.service;

import com.xin.entity.Dept;

/**
 * @author ：Student王心
 * @date ：Created in 2022/12/7 16:47
 * @description：
 * @modified By：
 * @version:
 */
public interface DeptService {

    /**
     * 根据id查询部门信息
     * @param id
     */
    public abstract Dept findById(Integer id);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    public abstract Integer delete(Integer id);

    /**
     *保存数据
     * @param dept
     * @return
     */
    public abstract Dept save(Dept dept);

    /**
     * 修改
     * @param dept
     * @return
     */
    public abstract Dept update(Dept dept);
}