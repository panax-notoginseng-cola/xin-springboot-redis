package com.xin.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xin.entity.Dept;

/**
 * @author ：Student王心
 * @date ：Created in 2022/12/7 16:45
 * @description：
 * @modified By：
 * @version:
 */
public interface DeptDao extends BaseMapper<Dept> {
}