package com.xin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.xin.dao")
public class Qy158SpringbootRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(Qy158SpringbootRedisApplication.class, args);
    }

}
