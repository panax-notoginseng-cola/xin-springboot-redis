package com.xin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：Student王心
 * @date ：Created in 2022/12/7 16:42
 * @description：
 * @modified By：
 * @version:
 */
@TableName(value = "tb_dept")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dept {

    @TableId
    private Integer id;

    private String name;
    private String address;
}