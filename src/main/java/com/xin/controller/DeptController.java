package com.xin.controller;

import com.xin.entity.Dept;
import com.xin.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ：Student王心
 * @date ：Created in 2022/12/7 16:51
 * @description：
 * @modified By：
 * @version:
 */
@RestController
@RequestMapping("dept")
public class DeptController {

    @Autowired
    private DeptService deptService;

    //restful风格: 根据不同的提交方式调用不同的方法。
    // get-查询 post-添加  delete-删除  put-修改
    @GetMapping("/{id}")
    public Dept getById(@PathVariable Integer id){
        return deptService.findById(id);
    }

    @DeleteMapping("/{id}")
    public Integer delete(@PathVariable Integer id) {
        return deptService.delete(id);
    }

    @PostMapping
    public Dept save(@RequestBody Dept dept){
        return deptService.save(dept);
    }

    @PutMapping
    public Dept update(@RequestBody Dept dept){
        return deptService.update(dept);
    }
}